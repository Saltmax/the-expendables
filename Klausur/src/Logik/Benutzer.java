package Logik;

public class Benutzer {
	//Initialisirung
	private String Benutzername;
	
	//Parametrisierter Konstruktor
	public Benutzer(String Benutzername)	{
		this.Benutzername = Benutzername;
	}

	/**
	 * @return the benutzername
	 */
	public String getBenutzername() {
		return Benutzername;
	}

	/**
	 * @param benutzername the benutzername to set
	 */
	public void setBenutzername(String benutzername) {
		Benutzername = benutzername;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Benutzername == null) ? 0 : Benutzername.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Benutzer other = (Benutzer) obj;
		if (Benutzername == null) {
			if (other.Benutzername != null)
				return false;
		} else if (!Benutzername.equals(other.Benutzername))
			return false;
		return true;
	}
	
}
