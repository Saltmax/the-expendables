package Logik;


public class Musik {
	//Initialisierung
	private String Bandname;
	private String Titelname;
	private String Genre;
	//Parametrisierter Konstruktor
	public Musik (String Titelname, String Bandname, String Genre) {
	this.Titelname = Titelname;
	this.Bandname = Bandname;
	this.Genre = Genre;
	}
	//Getter
	public String getBandname()	{
		return Bandname;
	}
	public String getTitelname()	{
		return Titelname;
	}
	public String getGenre()	{
		return Genre;
	}
	//Setter
	public void setBandname(String Bandname)	{
		this.Bandname = Bandname;
	}
	public void setTitelname(String Titelname)	{
		this.Titelname = Titelname;
	}
	public void setGenre(String Genre)	{
		this.Genre = Genre;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Musik [Bandname=" + Bandname + ", Titelname=" + Titelname + ", Genre=" + Genre + "]";
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Bandname == null) ? 0 : Bandname.hashCode());
		result = prime * result + ((Genre == null) ? 0 : Genre.hashCode());
		result = prime * result + ((Titelname == null) ? 0 : Titelname.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Musik other = (Musik) obj;
		if (Bandname == null) {
			if (other.Bandname != null)
				return false;
		} else if (!Bandname.equals(other.Bandname))
			return false;
		if (Genre == null) {
			if (other.Genre != null)
				return false;
		} else if (!Genre.equals(other.Genre))
			return false;
		if (Titelname == null) {
			if (other.Titelname != null)
				return false;
		} else if (!Titelname.equals(other.Titelname))
			return false;
		return true;
	}
}
