package Datenbank;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Das Programm erstellt eine Verbindung zu einer MySQL Datenbank her und lie�t
 * die Daten von einer Tabelle aus. Dies kann beliebig erweitert werden. Hierzu
 * muss eine Bibliothek f�r MySQL importiert werden und benutzt werden.
 * 
 */
public class MiniDataBaseApp {
	private String DRIVER = "com.mysql.jdbc.Driver";
	private String db_user = "root";
	private String db_pass = "";
	private String url = "jdbc:mysql://localhost/database expandables?";
	private Connection connection = null;

	/**
	 * Eine Verbindung zur Datenbank wird hergestellt und von einer Tabelle werden
	 * alle Eintr�ge ausgelesen.
	 * 
	 * @throws SQLException
	 *             Wenn keine Verbindung zu einer Datenbank hergestellt werden
	 *             konnte.
	 * @throws ClassNotFoundException
	 *             Wenn der Datenbanktreiber nicht gefunden werden konnte.
	 */
	public void createConnection() throws SQLException, ClassNotFoundException {
		// Treiber initialisieren
		Class.forName(DRIVER);
		// Verbindung herstellen.
		connection = DriverManager.getConnection(url, db_user, db_pass);
	}

	/**
	 * @throws SQLException
	 *             Wenn die Abfrage an der Datenbank nicht gemacht werden konnte.
	 */
	public JTable showAll(String t_musik) throws SQLException {
		// Statement mit Benennung der Tablle
		String query = "SELECT * FROM t_musik";
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(query);
		int columns = rs.getMetaData().getColumnCount();

		String[] columnsData = new String[columns];
		// Ich zeige die Tabellenspaltennamen an.
		for (int i = 0; i < columns; i++) {
			columnsData[i] = rs.getMetaData().getColumnLabel(i + 1);

		}

		// Ich zeige den Inhalt der Tabelle an. Normaler Weise w�rde man die
		// Ergebnisse in eine Liste schreiben und diese zur�ck liefern.
		DefaultTableModel tableModel = new DefaultTableModel(columnsData, 0);
		JTable table = new JTable(tableModel);
		while (rs.next()) {
			String[] temp = new String[columns];
			for (int i = 0; i < columns; i++) {
				temp[i] = rs.getString(i + 1);
			}
			tableModel.addRow(temp);
		}
		// Ich schlie�e die Streams wieder und gebe die Tabelle wieder frei.
		rs.close();
		stmt.close();
		return table;
	}

	/**
	 * Ich schlie�e die Verbindung zu der Datenbank und gebe den Port am Computer
	 * frei.
	 * 
	 * @throws SQLException
	 *             Wenn die Verbindung nicht geschlossen werden konnte.
	 */
	public void closeProgramm() throws SQLException {
		this.connection.close();
	}
}