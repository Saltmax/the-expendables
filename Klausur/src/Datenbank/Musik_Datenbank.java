package Datenbank;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

import Logik.Musik;

public class Musik_Datenbank {

	public void musik_Eintragen(Musik m) {
		// Parameter fuer Verbindungsaufbau definieren
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost/database expandables?";
		String user = "root";
		String password = "";
		String sql ="INSERT INTO t_musik (Titelname,Bandname,Genre) VALUES (?,?,?)";

		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			// SQL-Anweisungen ausfuehren
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, m.getTitelname());
			stmt.setString(2, m.getBandname());
			stmt.setString(3, m.getGenre());
			stmt.executeUpdate();
			
			/*
			 * Dieser quellcode abschnitt würde auch gehen,
			 * aber er ist unübersichtlich und nervig weil man auf die Anführungszeichen konzentrien muss.
			 * Statement stmt = con.createStatement();
			 * stmt.execute("INSERT INTO t_musik (Titelname,Bandname,Genre) VALUES (\"" + m.getTitelname() + "\",\""
					+ m.getBandname() + "\",\"" + m.getGenre() + "\")");
			*/
			con.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}