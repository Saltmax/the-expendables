package Datenbank;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import Logik.Benutzer;
public class Benutzer_Datenbank {
	public void benutzer_Eintragen(Benutzer b)	{
		//Parameter fuer Verbindungsaufbau definieren
		//String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost/database expandables?";
		String user = "root";
		String password = "";
		
		try	{
			// JDBC-Treiber laden
			Class.forName("com.mysql.jdbc.Driver");
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			// SQL-Anweisungen ausfuehren
			Statement stmt = con.createStatement();
			stmt.execute("INSERT INTO t_benutzer (Benutzername) VALUES (\"" + b.getBenutzername() + "\" )");
			con.close();
		} catch (ClassNotFoundException | SQLException e)	{
			e.printStackTrace();
		}
	}
}
