-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.1.28-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win32
-- HeidiSQL Version:             9.4.0.5188
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Exportiere Datenbank Struktur für database expandables
CREATE DATABASE IF NOT EXISTS `database expandables` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `database expandables`;

-- Exportiere Struktur von Tabelle database expandables.t_benutzer
CREATE TABLE IF NOT EXISTS `t_benutzer` (
  `Benutzer_ID` int(11) NOT NULL,
  `Benutzername` varchar(50) DEFAULT NULL,
  `Hausname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Benutzer_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabelle mit Benutzerinformationen';

-- Daten Export vom Benutzer nicht ausgewählt
-- Exportiere Struktur von Tabelle database expandables.t_musik
CREATE TABLE IF NOT EXISTS `t_musik` (
  `Titelname` varchar(50) NOT NULL,
  `Bandname` varchar(50) DEFAULT NULL,
  `Genre` varchar(50) DEFAULT NULL,
  `Titel_ID` int(11) DEFAULT NULL,
  `Voteanzahl` int(11) DEFAULT NULL,
  PRIMARY KEY (`Titelname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabelle mit Musiktiteln,Bandname und Genre';

-- Daten Export vom Benutzer nicht ausgewählt
-- Exportiere Struktur von Tabelle database expandables.t_vote
CREATE TABLE IF NOT EXISTS `t_vote` (
  `Voteanzahl` int(11) NOT NULL,
  `Benutzer_ID` int(11) DEFAULT NULL,
  `Titelname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Voteanzahl`),
  KEY `FK_t_vote_t_benutzer` (`Benutzer_ID`),
  KEY `FK_t_vote_t_musik` (`Titelname`),
  CONSTRAINT `FK_t_vote_t_benutzer` FOREIGN KEY (`Benutzer_ID`) REFERENCES `t_benutzer` (`Benutzer_ID`),
  CONSTRAINT `FK_t_vote_t_musik` FOREIGN KEY (`Titelname`) REFERENCES `t_musik` (`Titelname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabelle für Votes, Benutzer_ID''s und Titel_ID''s';

-- Daten Export vom Benutzer nicht ausgewählt
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
