package Design;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

import Logik.Benutzer;
import Logik.Benutzer_Logic;

public class LogginFenster extends JFrame {

	private JPanel contentPane;
	private JTextField txt;
	private Benutzer_Logic blogic = new Benutzer_Logic();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LogginFenster frame = new LogginFenster();
					frame.setTitle("Loggin");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LogginFenster() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JLabel lblMusikvote = new JLabel("MUSIKVOTE");
		lblMusikvote.setFont(new Font("Tahoma", Font.PLAIN, 27));
		lblMusikvote.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblMusikvote, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(	3, 1, 0, 0));
		
		JLabel lblGastname = new JLabel();
		lblGastname.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblGastname.setText("Gastname");
		lblGastname.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblGastname);
		
		JButton btnEinloggen = new JButton("Einloggen");
		btnEinloggen.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnEinloggen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonLoginClicked();
			}
		});
		
		txt = new JTextField();
		txt.setHorizontalAlignment(SwingConstants.CENTER);
		txt.setFont(new Font("Tahoma", Font.PLAIN, 25));
		panel.add(txt);
		txt.setColumns(10);
		panel.add(btnEinloggen);
	}
	
	static String benutzerNameStatic;
	
	public void buttonLoginClicked()	{
		String Benutzername = txt.getText();
		benutzerNameStatic=Benutzername;
		Benutzer b = new Benutzer(Benutzername);
		blogic.benutzer_Eintragen(b);
		Musikvoting frame = new Musikvoting();
		frame.setVisible(true);
		this.dispose();
	}
	
	public String BuntzerAnzeigen()	{
	return benutzerNameStatic;
	}

}
