package Design;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JEditorPane;

import Datenbank.MiniDataBaseApp;

import java.awt.Panel;

public class Musikvoting extends JFrame {

	private JPanel contentPane;
	private LogginFenster lf = new LogginFenster();
	private JTable table;
	MiniDataBaseApp app = new MiniDataBaseApp();

	private JScrollPane scrollpane;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Musikvoting frame = new Musikvoting();
					frame.setTitle("Startseite");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Musikvoting() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 841, 611);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new GridLayout(1, 0, 0, 0));

		JLabel lblNewLabel = new JLabel("Benutzer: " + lf.BuntzerAnzeigen());
		panel.add(lblNewLabel);

		JButton btn_ZP = new JButton("Zeige Playlist");
		btn_ZP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				votlisteAnzeigen();
			}
		});
		panel.add(btn_ZP);

		JButton btn_PA = new JButton("Playlist Aktualisieren");
		btn_PA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				playlistupdate();
			}
		});
		panel.add(btn_PA);

		JButton btn_MH = new JButton("Musik Hinzuf\u00FCgen");
		contentPane.add(btn_MH, BorderLayout.SOUTH);
		btn_MH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonMusik_HinzufuegenClicked();
			}
		});

		scrollpane = new JScrollPane();
		contentPane.add(scrollpane, BorderLayout.CENTER);
		playlistupdate(); 
	}

	private void playlistupdate() {
		try {
			// Verbindung herstellen
			app.createConnection();
			// Ergebnisse anzeigen.
			scrollpane.setViewportView(app.showAll("pma_history"));
			scrollpane.invalidate();
			scrollpane.revalidate();
			// Verbindung schlie�en
			app.closeProgramm();
		} catch (SQLException e) {
			System.err.println("SQL Fehler - " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("Der Datenbank treiber wurde nicht gefunden. - " + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	public void buttonMusik_HinzufuegenClicked() {
		MusikHinzufuegen frame = new MusikHinzufuegen();
		frame.setVisible(true);
	}
	public void votlisteAnzeigen()	{
		Playlist frame = new Playlist();
		frame.setVisible(true);
	}
}
