package Design;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;

import Logik.*;

public class MusikHinzufuegen extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldGenre;
	private JTextField textFieldBandname;
	private JTextField textFieldTitelname;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private Musik_Logic mlogic = new Musik_Logic();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MusikHinzufuegen frame = new MusikHinzufuegen();
					frame.setTitle("Hinzufügen");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MusikHinzufuegen() {
		// setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setBounds(100, 100, 815, 530);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(7, 0, 0, 0));

		lblNewLabel = new JLabel("Titelname:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel.setVerticalAlignment(SwingConstants.BOTTOM);
		contentPane.add(lblNewLabel);

		textFieldTitelname = new JTextField();
		textFieldTitelname.setFont(new Font("Tahoma", Font.PLAIN, 25));
		contentPane.add(textFieldTitelname);
		textFieldTitelname.setColumns(10);

		lblNewLabel_1 = new JLabel("Bandname:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel_1.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
		contentPane.add(lblNewLabel_1);

		textFieldBandname = new JTextField();
		textFieldBandname.setFont(new Font("Tahoma", Font.PLAIN, 25));
		contentPane.add(textFieldBandname);
		textFieldBandname.setColumns(10);

		lblNewLabel_2 = new JLabel("Genre:");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel_2.setVerticalAlignment(SwingConstants.BOTTOM);
		contentPane.add(lblNewLabel_2);

		textFieldGenre = new JTextField();
		textFieldGenre.setFont(new Font("Tahoma", Font.PLAIN, 25));
		contentPane.add(textFieldGenre);
		textFieldGenre.setColumns(10);

		JButton btnEintragen = new JButton("Eintragen");
		btnEintragen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonEintragenClicked();
			}
		});
		btnEintragen.setFont(new Font("Tahoma", Font.PLAIN, 25));
		contentPane.add(btnEintragen);
	}

	protected void buttonEintragenClicked() {
		String Titelname = textFieldTitelname.getText();
		String Bandname = textFieldBandname.getText();
		String Genre = textFieldGenre.getText();
		Musik m = new Musik(Titelname, Bandname, Genre);
		mlogic.musik_Eintragen(m);
		this.dispose();
	}

}
